package cz.cvut.fel.sum;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Action {
	private int id;
	@JsonProperty("request_index")
	private Request request;
	@JsonProperty("node")
	private Coordinate node;

	@JsonProperty("type")
	private ActionType actionType;

	@JsonProperty("min_time")
	private int minTime;

	@JsonProperty("max_time")
	private int maxTime;
	@JsonProperty("service_time")
	private int serviceTime;

	public Action(int id, Coordinate node, int minTime, int maxTime, ActionType actionType, int serviceTime) {
		this.id = id;
		this.node = node;
		this.minTime = minTime;
		this.maxTime = maxTime;
		this.actionType = actionType;
		this.serviceTime = serviceTime;
	}

	public int getId() {
		return id;
	}

	public Coordinate getNode() {
		return node;
	}

	public int getMinTime() {
		return minTime;
	}

	public int getMaxTime() {
		return maxTime;
	}

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public ActionType getActionType() {
		return actionType;
	}

	public int getServiceTime() {
		return serviceTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Action action = (Action) o;
		return id == action.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}

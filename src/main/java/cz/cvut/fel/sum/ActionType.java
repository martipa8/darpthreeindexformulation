package cz.cvut.fel.sum;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ActionType {
	@JsonProperty("pickup") PICKUP,
	@JsonProperty("dropoff") DROP_OFF
}

package cz.cvut.fel.sum;

public interface Coordinate {
	double getX();
	double getY();
}

package cz.cvut.fel.sum;

import java.util.Objects;

public class CordeauNode implements Coordinate {
	private double x;
	private double y;

	public CordeauNode(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		CordeauNode that = (CordeauNode) o;
		return Double.compare(that.x, x) == 0 &&
				Double.compare(that.y, y) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public String toString() {
		return "(" + x + ", " + y + ")";
	}
}

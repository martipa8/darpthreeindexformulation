package cz.cvut.fel.sum;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CordeauReader {

	public DARPInstance Read(String filepath) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(filepath));

			String[] firstLine = reader.readLine().split("\\s+");
			int numVehicles = Integer.parseInt(firstLine[0]);
			int numRequests = Integer.parseInt(firstLine[1]);
			int maxRouteDuration = Integer.parseInt(firstLine[2]);
			int vehicleCapacity = Integer.parseInt(firstLine[3]);
			int maxRideTime = Integer.parseInt(firstLine[4]);

			String[] secondLine = reader.readLine().trim().split("\\s+");
			double depotX = Double.parseDouble(secondLine[1]);
			double depotY = Double.parseDouble(secondLine[2]);
			CordeauNode depotNode = new CordeauNode(depotX, depotY);

			List<Vehicle> vehicles = new ArrayList<Vehicle>();

			for (int i = 0; i < numVehicles; i++) {
				vehicles.add(new Vehicle(i, depotNode, vehicleCapacity));
			}

			List<OriginAction> originActions = new ArrayList<>();
			List<Request> requests = new ArrayList<>();

			TravelTimeProvider travelTimeProvider = new EuclidianTravelTimeProvider(60);

			String line = reader.readLine();
			int counter = 0;
			int index = 0;

			Set<Coordinate> nodes = new HashSet<>();

			while (line != null) {
				String[] lineArr = line.trim().split("\\s+");
				int id = Integer.parseInt(lineArr[0]);
				double x = Double.parseDouble(lineArr[1]);
				double y = Double.parseDouble(lineArr[2]);
				int serviceTime = Integer.parseInt(lineArr[3]);
				int origin = Integer.parseInt(lineArr[4]);
				int minTime = Integer.parseInt(lineArr[5]);
				int maxTime = Integer.parseInt(lineArr[6]);
				Coordinate node = new CordeauNode(x, y);

				nodes.add(node);

				if (origin == 1) {
					originActions.add(new OriginAction(id, node, minTime*60, maxTime*60, serviceTime*60));
				}
				else {
					int minTravelTime = travelTimeProvider.getTravelTime(originActions.get(counter).getPickUpNode() ,node);

					OriginAction oa = originActions.get(counter);
					Action pickUpAction = new Action(oa.getPickUpId(), oa.getPickUpNode(), oa.getPickUpMinTime(),  oa.getPickUpMaxTime(), ActionType.PICKUP, oa.getPickUpServiceTime());
					Action dropOffAction = new Action(id, node, minTime*60, maxTime*60, ActionType.DROP_OFF, serviceTime*60);

					Request request = new Request(index, pickUpAction, dropOffAction, minTravelTime);
					pickUpAction.setRequest(request);
					dropOffAction.setRequest(request);

					requests.add(request);
					counter++;
					index++;
				}
				line = reader.readLine();
			}

			return new DARPInstance(requests, vehicles, nodes, travelTimeProvider, maxRouteDuration * 60, maxRideTime * 60);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}

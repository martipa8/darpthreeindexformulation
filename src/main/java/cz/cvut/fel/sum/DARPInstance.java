package cz.cvut.fel.sum;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DARPInstance {

	private List<Request> requests;
	private List<Vehicle> vehicles;
	private Set<Coordinate> nodes;
	private TravelTimeProvider travelTimeProvider;
	private int maxRouteDuration;
	private int maxRideTime;

	private List<Action> VAction;
	private List<Action> pickUpNodesAction;
	private List<Action> dropOffNodesAction;
	private Action startNode;
	private Action endNode;

	public DARPInstance(List<Request> requests, List<Vehicle> vehicles, Set<Coordinate> nodes, TravelTimeProvider travelTimeProvider, int maxRouteDuration, int maxRideTime) {
		this.requests = requests;
		this.vehicles = vehicles;
		this.nodes = nodes;
		this.travelTimeProvider = travelTimeProvider;
		this.maxRouteDuration = maxRouteDuration;
		this.maxRideTime = maxRideTime;

		this.startNode = new Action(-1, vehicles.get(0).getInitialPosition(), 0, 86400, ActionType.PICKUP, 0);
		this.endNode = new Action(-2, vehicles.get(0).getInitialPosition(), 0, 86400, ActionType.DROP_OFF, 0);
		createVAction();
	}

	private void createVAction(){
		VAction = new ArrayList<>();

		pickUpNodesAction = new ArrayList<>();
		dropOffNodesAction = new ArrayList<>();

		for (Request request : requests) {
			pickUpNodesAction.add(request.getPickUpAction());
			dropOffNodesAction.add(request.getDropOffAction());
		}

		VAction.add(startNode);
		VAction.addAll(pickUpNodesAction);
		VAction.addAll(dropOffNodesAction);
		VAction.add(endNode);
	}

	public List<Request> getRequests() {
		return requests;
	}

	public List<Vehicle> getVehicles() {
		return vehicles;
	}

	public Set<Coordinate> getNodes() {
		return nodes;
	}

	public TravelTimeProvider getTravelTimeProvider() {
		return travelTimeProvider;
	}

	public int getMaxRouteDuration() {
		return maxRouteDuration;
	}

	public int getMaxRideTime() {
		return maxRideTime;
	}

	public List<Action> getVAction() {
		return VAction;
	}

	public List<Action> getPickUpNodesAction() {
		return pickUpNodesAction;
	}

	public List<Action> getDropOffNodesAction() {
		return dropOffNodesAction;
	}

	public Action getStartNode() {
		return startNode;
	}

	public Action getEndNode() {
		return endNode;
	}
}

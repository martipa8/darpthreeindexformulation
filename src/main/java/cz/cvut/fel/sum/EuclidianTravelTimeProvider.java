package cz.cvut.fel.sum;

public class EuclidianTravelTimeProvider implements TravelTimeProvider {

	private int coordinateResolution;

	public EuclidianTravelTimeProvider(int coordinateResolution) {
		this.coordinateResolution = coordinateResolution;
	}


	public int getTravelTime(Coordinate fromPosition, Coordinate toPosition) {
		double a = Math.pow(fromPosition.getX() - toPosition.getX(), 2);
		double b = Math.pow(fromPosition.getY() - toPosition.getY(), 2);

		double distance = Math.sqrt(a + b);
		int travelTime = (int) Math.round(distance * coordinateResolution);

		return travelTime;
	}
}

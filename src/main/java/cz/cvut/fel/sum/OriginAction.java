package cz.cvut.fel.sum;

public class OriginAction {
	private int pickUpId;
	private Coordinate pickUpNode;
	private int pickUpMinTime;
	private int pickUpMaxTime;
	private int pickUpServiceTime;

	public OriginAction(int pickUpId, Coordinate pickUpNode, int pickUpMinTime, int pickUpMaxTime, int pickUpServiceTime) {
		this.pickUpId = pickUpId;
		this.pickUpNode = pickUpNode;
		this.pickUpMinTime = pickUpMinTime;
		this.pickUpMaxTime = pickUpMaxTime;
		this.pickUpServiceTime = pickUpServiceTime;
	}

	public int getPickUpId() {
		return pickUpId;
	}

	public Coordinate getPickUpNode() {
		return pickUpNode;
	}

	public int getPickUpMinTime() {
		return pickUpMinTime;
	}

	public int getPickUpMaxTime() {
		return pickUpMaxTime;
	}

	public int getPickUpServiceTime() {
		return pickUpServiceTime;
	}
}

package cz.cvut.fel.sum;

import com.fasterxml.jackson.annotation.JsonValue;

public class Request {
	@JsonValue()
	private int index;

	private Action pickUpAction;
	private Action dropOffAction;
	private int minTravelTime;

	public Request(int index, Action pickUpAction, Action dropOffAction, int minTravelTime) {
		this.index = index;
		this.pickUpAction = pickUpAction;
		this.dropOffAction = dropOffAction;
		this.minTravelTime = minTravelTime;
	}

	public int getIndex() {
		return index;
	}

	public Action getPickUpAction() {
		return pickUpAction;
	}

	public Action getDropOffAction() {
		return dropOffAction;
	}

	public int getMinTravelTime() {
		return minTravelTime;
	}
}


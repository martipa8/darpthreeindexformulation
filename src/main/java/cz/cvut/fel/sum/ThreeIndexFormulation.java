package cz.cvut.fel.sum;

import cz.cvut.fel.sum.solution.SolutionToJSON;
import gurobi.*;

import java.util.*;

public class ThreeIndexFormulation {

	public void CreateFormulation(String filePath, String solutionPath) {
		try {
			DARPInstance darpInstance = new CordeauReader().Read(filePath);
			GRBEnv env = new GRBEnv(true);
			env.set("logFile", "mip1.log");
			env.start();
			GRBModel model = new GRBModel(env);

			List<Vehicle> vehicles = darpInstance.getVehicles();
			List<Action> nodes1 = darpInstance.getVAction();
			List<Action> pickUpNodesAction = darpInstance.getPickUpNodesAction();

			Map<VarKey, GRBVar> vars = new HashMap<>();

			for (Vehicle vehicle : vehicles) {
				for (Action j : pickUpNodesAction) {
					Action i = darpInstance.getStartNode();
					String name = String.format("Arc:IdStart=%s,IdEnd=%s,Vehicle=%s,StartNode=%s,EndNode=%s",
							i.getId(), j.getId(), vehicle.getIndex(), i.getNode().toString(), j.getNode().toString());
					vars.put(new VarKey(i, j, vehicle), model.addVar(0.0, 1.0, 0.0, GRB.BINARY, name));
				}
				for (Action i : darpInstance.getDropOffNodesAction()) {
					Action j = darpInstance.getEndNode();
					String name = String.format("Arc:IdStart=%s,IdEnd=%s,Vehicle=%s,StartNode=%s,EndNode=%s",
							i.getId(), j.getId(), vehicle.getIndex(), i.getNode().toString(), j.getNode().toString());
					vars.put(new VarKey(i, j, vehicle), model.addVar(0.0, 1.0, 0.0, GRB.BINARY, name));
				}

				for (int i = 1; i < nodes1.size() -1; i++) {
					for (int j = 1; j < nodes1.size() -1; j++) {
						if (i != j) {
							String name = String.format("Arc:IdStart=%s,IdEnd=%s,Vehicle=%s,StartNode=%s,EndNode=%s",
									nodes1.get(i).getId(), nodes1.get(j).getId(), vehicle.getIndex(), nodes1.get(i).getNode().toString(), nodes1.get(j).getNode().toString());
							vars.put(new VarKey(nodes1.get(i), nodes1.get(j), vehicle), model.addVar(0.0, 1.0, 0.0, GRB.BINARY, name));
						}
					}
				}
			}

			Map<VertexCarKey, GRBVar> vertexServiceStart = new HashMap<>();
			for (Vehicle vehicle : vehicles) {
				for (Action node : nodes1) {
					String name = String.format("ServiceStart: Vehicle = %s, Id = %s", vehicle, node.getId());
					vertexServiceStart.put(new VertexCarKey(node, vehicle), model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, name));
				}
			}

			Map<VertexCarKey, GRBVar> loadOfVehicle = new HashMap<>();
			for (Vehicle vehicle : vehicles) {
				for (Action node : nodes1) {
					String name = String.format("VehicleLoad: Vehicle = %s, Id = %s", vehicle, node.getId());
					loadOfVehicle.put(new VertexCarKey(node, vehicle), model.addVar(0.0, GRB.INFINITY, 0.0, GRB.INTEGER, name));
				}
			}

			Map<VertexCarKey, GRBVar> rideTimeOfUser = new HashMap<>();
			for (Vehicle vehicle : vehicles) {
				for (Action node : pickUpNodesAction) {
					String name = String.format("RideTime: Vehicle = %s, Id = %s", vehicle, node.getId());
					rideTimeOfUser.put(new VertexCarKey(node, vehicle), model.addVar(0.0, GRB.INFINITY, 0.0, GRB.CONTINUOUS, name));
				}
			}

			//Objective
			GRBLinExpr expr = new GRBLinExpr();
			for (VarKey key : vars.keySet()) {
				int time = darpInstance.getTravelTimeProvider().getTravelTime(key.getPickUp().getNode(), key.getDropOff().getNode());
				expr.addTerm(time, vars.get(key));
			}
			model.setObjective(expr, GRB.MINIMIZE);

			//Constraints
			GRBLinExpr constraint;
			GRBLinExpr constraint2;

			//Constraint 2
			for (Action i : pickUpNodesAction) {
				constraint = new GRBLinExpr();
				for (Action j : nodes1) {
					for (Vehicle vehicle : vehicles) {
						if (vars.containsKey(new VarKey(i, j, vehicle))) {
							constraint.addTerm(1, vars.get(new VarKey(i, j, vehicle)));
						}
					}
				}
				model.addConstr(constraint, GRB.EQUAL, 1, String.format("Constraint 2: i = %s", i.getNode()));
			}

			//Constraint 3
			for (Vehicle vehicle : vehicles) {
				constraint = new GRBLinExpr();
				constraint2 = new GRBLinExpr();
				for (Action action : nodes1) {
					if (vars.containsKey(new VarKey(darpInstance.getStartNode(), action, vehicle))) {
						constraint.addTerm(1, vars.get(new VarKey(darpInstance.getStartNode(), action, vehicle)));
					}
					if (vars.containsKey(new VarKey(action, darpInstance.getEndNode(), vehicle))) {
						constraint2.addTerm(1, vars.get(new VarKey(action, darpInstance.getEndNode(), vehicle)));
					}
				}
				model.addConstr(constraint, GRB.EQUAL, 1, String.format("Constraint 3.1: k = %s", vehicle));
				model.addConstr(constraint2, GRB.EQUAL, 1, String.format("Constraint 3.2: k = %s", vehicle));
			}

			//Constraint 4
			for (Action i : pickUpNodesAction) {
				for (Vehicle vehicle : vehicles) {
					constraint = new GRBLinExpr();
					constraint2 = new GRBLinExpr();
					for (Action j : nodes1) {
						if (vars.containsKey(new VarKey(i, j, vehicle))) {
							constraint.addTerm(1, vars.get(new VarKey(i, j, vehicle)));
						}
						if (vars.containsKey(new VarKey(i.getRequest().getDropOffAction(), j, vehicle))) {
							constraint2.addTerm(1, vars.get(new VarKey(i.getRequest().getDropOffAction(), j, vehicle)));
						}
					}
					model.addConstr(constraint, GRB.EQUAL, constraint2, String.format("Constraint 4: i = %s, k = %s", i.getNode(), vehicle));
				}
			}

			//Constraint 5
			for (Vehicle vehicle : vehicles) {
				for (int i = 1; i < nodes1.size() - 1; i++) {
					constraint = new GRBLinExpr();
					constraint2 = new GRBLinExpr();
					for (Action j : nodes1) {
						if (vars.containsKey(new VarKey(j, nodes1.get(i), vehicle))) {
							constraint.addTerm(1, vars.get(new VarKey(j, nodes1.get(i), vehicle)));
						}
						if (vars.containsKey(new VarKey(nodes1.get(i), j, vehicle))) {
							constraint2.addTerm(1, vars.get(new VarKey(nodes1.get(i), j, vehicle)));
						}
					}
					model.addConstr(constraint, GRB.EQUAL, constraint2, String.format("Constraint 5: k = %s, i = %s", vehicle, nodes1.get(i).getNode()));
				}
			}
			//Constraint 6
			for (Action i : nodes1) {
				for (Action j : nodes1) {
					for (Vehicle vehicle : vehicles) {
						if (vars.containsKey(new VarKey(i, j, vehicle))) {
							constraint = new GRBLinExpr();
							constraint.addTerm(1, vertexServiceStart.get(new VertexCarKey(j, vehicle)));

							constraint2 = new GRBLinExpr();
							constraint2.addTerm(1, vertexServiceStart.get(new VertexCarKey(i, vehicle)));
							constraint2.addConstant(i.getServiceTime());
							constraint2.addConstant(darpInstance.getTravelTimeProvider().getTravelTime(i.getNode(), j.getNode()));
							//TODO: change M to better value
							int M = 86000;
							constraint2.addConstant(-M);
							constraint2.addTerm(M, vars.get(new VarKey(i, j, vehicle)));

							String name = String.format("Constraint 6: k = %s, i = %s, j = %s", vehicle, i.getNode(), j.getNode());
							model.addConstr(constraint, GRB.GREATER_EQUAL, constraint2, name);
						}
					}
				}
			}

			//Constraint 7
			for (Action i : nodes1) {
				for (Action j : nodes1) {
					for (Vehicle vehicle : vehicles) {
						if (vars.containsKey(new VarKey(i, j, vehicle))) {
							constraint = new GRBLinExpr();
							constraint.addTerm(1, loadOfVehicle.get(new VertexCarKey(j, vehicle)));

							constraint2 = new GRBLinExpr();
							constraint2.addTerm(1, loadOfVehicle.get(new VertexCarKey(i, vehicle)));

							if (j.getId() < 0) {
								//							constraint2.addConstant(0);
							}
							else if (j.getActionType() == ActionType.DROP_OFF) {
								constraint2.addConstant(-1);
							}
							else {
								constraint2.addConstant(1);
							}
							//TODO: change M to better value
							int M = 10;
							constraint2.addConstant(-M);
							constraint2.addTerm(M, vars.get(new VarKey(i, j, vehicle)));

							String name = String.format("Constraint7:k=%s,i=%s,j=%s", vehicle, i.getId(), j.getId());
							model.addConstr(constraint, GRB.GREATER_EQUAL, constraint2, name);
						}
					}
				}
			}

			//Constraint 8
			for (Vehicle vehicle : vehicles) {
				for (Action action : pickUpNodesAction) {
					constraint = new GRBLinExpr();
					constraint.addTerm(1, vertexServiceStart.get(new VertexCarKey(action.getRequest().getDropOffAction(), vehicle)));
					constraint.addTerm(-1, vertexServiceStart.get(new VertexCarKey(action, vehicle)));
					constraint.addConstant(-1 * action.getServiceTime());

					constraint2 = new GRBLinExpr();
					constraint2.addTerm(1, rideTimeOfUser.get(new VertexCarKey(action, vehicle)));

					String name = String.format("Constraint 8: k = %s, i = %s", vehicle, action.getNode());
					model.addConstr(constraint2, GRB.EQUAL, constraint, name);
				}
			}

			//Constraint 9
			for (Vehicle vehicle : vehicles) {
				constraint = new GRBLinExpr();
				constraint.addTerm(1, vertexServiceStart.get(new VertexCarKey(darpInstance.getEndNode(), vehicle)));
				constraint.addTerm(-1, vertexServiceStart.get(new VertexCarKey(darpInstance.getStartNode(), vehicle)));

				String name = String.format("Constraint 9: k = %s", vehicle);
				model.addConstr(constraint, GRB.LESS_EQUAL, darpInstance.getMaxRouteDuration(), name);

				//TODO: my const
				String name2 = String.format("Constraint 9.2: k = %s", vehicle);
				model.addConstr(0, GRB.LESS_EQUAL, constraint, name2);
			}

			//Constraint 10
			for (Vehicle vehicle : vehicles) {
				for (Action action : nodes1) {
					constraint = new GRBLinExpr();
					constraint.addTerm(1, vertexServiceStart.get(new VertexCarKey(action, vehicle)));
					String name = String.format("Constraint 10.1: k=%s,i=%s", vehicle, action.getNode());
					model.addConstr(action.getMinTime(), GRB.LESS_EQUAL, constraint, name);

					String name2 = String.format("Constraint 10.2: k=%s,i=%s", vehicle, action.getNode());
					model.addConstr(constraint, GRB.LESS_EQUAL, action.getMaxTime(), name2);
				}
			}

			//Constraint 11
			for (Vehicle vehicle : vehicles) {
				for (Action action : pickUpNodesAction) {
					constraint = new GRBLinExpr();
					constraint.addTerm(1, rideTimeOfUser.get(new VertexCarKey(action, vehicle)));

					int t = action.getRequest().getMinTravelTime();

					String name = String.format("Constraint 11.1: k = %s, i = %s", vehicle, action.getNode());
					model.addConstr(t, GRB.LESS_EQUAL, constraint, name);

					String name2 = String.format("Constraint 11.2: k = %s, i = %s", vehicle, action.getNode());
					model.addConstr(constraint, GRB.LESS_EQUAL, darpInstance.getMaxRideTime(), name2);
				}
			}

			//Constraint 12
			for (Vehicle vehicle : vehicles) {
				for (Action i : nodes1) {
					constraint = new GRBLinExpr();
					int firstConst = 0;

					if (i.getActionType() == ActionType.PICKUP && i.getId() >= 0) {
						firstConst = 1;
					}

					GRBVar w = loadOfVehicle.get(new VertexCarKey(i, vehicle));
					constraint.addTerm(1, w);
					String name = String.format("Constraint 12.1: k = %s, i = %s", vehicle, i.getNode());
					model.addConstr(firstConst, GRB.LESS_EQUAL, constraint, name);

					int secondConst = vehicle.getCapacity();
					if (i.getActionType() == ActionType.DROP_OFF && i.getId() >= 0) {
						secondConst--;
					}

					String name2 = String.format("Constraint 12.2: k = %s, i = %s", vehicle, i.getNode());
					model.addConstr(w, GRB.LESS_EQUAL, secondConst, name2);
				}
			}

			model.write("myModel.lp");
			model.optimize();

			SolutionToJSON solutionToJSON = new SolutionToJSON(vars, vertexServiceStart, loadOfVehicle, rideTimeOfUser, darpInstance);
			solutionToJSON.convertToJSON(solutionPath);

			model.dispose();
			env.dispose();

		}
		catch (GRBException e) {
			e.printStackTrace();
		}
	}
}

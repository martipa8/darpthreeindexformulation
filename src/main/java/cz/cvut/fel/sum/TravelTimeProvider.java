package cz.cvut.fel.sum;

public interface TravelTimeProvider {

	int getTravelTime(Coordinate fromPosition, Coordinate toPosition);
}

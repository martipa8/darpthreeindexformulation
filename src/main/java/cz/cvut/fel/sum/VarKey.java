package cz.cvut.fel.sum;

import java.util.Objects;

public class VarKey {
	private Action pickUp;
	private Action dropOff;
	private Vehicle vehicle;

	public VarKey(Action pickUp, Action dropOff, Vehicle vehicle) {
		this.pickUp = pickUp;
		this.dropOff = dropOff;
		this.vehicle = vehicle;
	}

	public Action getPickUp() {
		return pickUp;
	}

	public Action getDropOff() {
		return dropOff;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		VarKey varKey = (VarKey) o;
		return pickUp.equals(varKey.pickUp) &&
				dropOff.equals(varKey.dropOff) &&
				vehicle.equals(varKey.vehicle);
	}

	@Override
	public int hashCode() {
		return Objects.hash(pickUp, dropOff, vehicle);
	}
}

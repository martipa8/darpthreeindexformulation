package cz.cvut.fel.sum;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Vehicle {
	private int index;
	@JsonProperty("init_position")
	private CordeauNode initialPosition;
	private int capacity;

	public Vehicle(int index, CordeauNode initialPosition, int capacity) {
		this.index = index;
		this.initialPosition = initialPosition;
		this.capacity = capacity;
	}

	public int getIndex() {
		return index;
	}

	public CordeauNode getInitialPosition() {
		return initialPosition;
	}

	public int getCapacity() {
		return capacity;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Vehicle vehicle = (Vehicle) o;
		return index == vehicle.index;
	}

	@Override
	public int hashCode() {
		return Objects.hash(index);
	}

	@Override
	public String toString() {
		return Integer.toString(index);
	}
}

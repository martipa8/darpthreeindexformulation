package cz.cvut.fel.sum;

import java.util.Objects;

public class VertexCarKey {
	private Action vertex;
	private Vehicle vehicle;

	public VertexCarKey(Action vertex, Vehicle vehicle) {
		this.vertex = vertex;
		this.vehicle = vehicle;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		VertexCarKey that = (VertexCarKey) o;
		return vertex.equals(that.vertex) &&
				vehicle.equals(that.vehicle);
	}

	@Override
	public int hashCode() {
		return Objects.hash(vertex, vehicle);
	}
}

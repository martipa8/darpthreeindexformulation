package cz.cvut.fel.sum.solution;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import cz.cvut.fel.sum.Action;

@JsonPropertyOrder({"arrivalTime", "departureTime", "action"})
public class ActionWrapper {
    @JsonProperty("arrival_time")
    private int arrivalTime;
    @JsonProperty("departure_time")
    private int departureTime;
    private Action action;

    public ActionWrapper(Action action) {
        this.action = action;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(int departureTime) {
        this.departureTime = departureTime;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}

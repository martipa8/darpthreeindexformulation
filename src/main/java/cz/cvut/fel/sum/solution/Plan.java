package cz.cvut.fel.sum.solution;

import cz.cvut.fel.sum.Vehicle;

import java.util.List;

public class Plan {
	private int cost;
	private Vehicle vehicle;
	private int departureTime;
	private int arrivalTime;
	private List<ActionWrapper> actions;

	public Plan(int cost, Vehicle vehicle, int departureTime, int arrivalTime, List<ActionWrapper> actions) {
		this.cost = cost;
		this.vehicle = vehicle;
		this.departureTime = departureTime;
		this.arrivalTime = arrivalTime;
		this.actions = actions;
	}

	public int getCost() {
		return cost;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public int getDepartureTime() {
		return departureTime;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public List<ActionWrapper> getActions() {
		return actions;
	}
}

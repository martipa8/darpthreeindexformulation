package cz.cvut.fel.sum.solution;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import cz.cvut.fel.sum.solution.Plan;

import java.util.List;

@JsonPropertyOrder({"cost", "costMinutes", "plans"})
public class Solution {
	private int cost;
	@JsonProperty("cost_minutes")
	private int costMinutes;
	private List<Plan> plans;

	public Solution(List<Plan> plans) {
		this.plans = plans;
		countCost();
	}

	public void countCost(){
		cost = 0;
		for (Plan plan : plans) {
			cost += plan.getCost();
		}
	}

	public int getCost() {
		return cost;
	}

	public int getCostMinutes() {
		return Math.round(cost/60);
	}

	public List<Plan> getPlans() {
		return plans;
	}
}

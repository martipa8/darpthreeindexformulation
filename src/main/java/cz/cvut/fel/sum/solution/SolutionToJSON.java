package cz.cvut.fel.sum.solution;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.sum.*;
import gurobi.GRB;
import gurobi.GRBException;
import gurobi.GRBVar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SolutionToJSON {
	Map<VarKey, GRBVar> vars;
	Map<VertexCarKey, GRBVar> vertexServiceStart;
	Map<VertexCarKey, GRBVar> loadOfVehicle;
	Map<VertexCarKey, GRBVar> rideTimeOfUser;
	DARPInstance darpInstance;

	public SolutionToJSON(Map<VarKey, GRBVar> vars, Map<VertexCarKey, GRBVar> vertexServiceStart, Map<VertexCarKey, GRBVar> loadOfVehicle, Map<VertexCarKey, GRBVar> rideTimeOfUser, DARPInstance darpInstance) {
		this.vars = vars;
		this.vertexServiceStart = vertexServiceStart;
		this.loadOfVehicle = loadOfVehicle;
		this.rideTimeOfUser = rideTimeOfUser;
		this.darpInstance = darpInstance;
	}

	public void convertToJSON(String path) throws GRBException {
		List<Plan> plans = new ArrayList<>();

		for (Vehicle vehicle : darpInstance.getVehicles()) {
			plans.add(createPlan(vehicle));
		}

		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.writeValue(new File(path), new Solution(plans));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Plan createPlan(Vehicle vehicle) throws GRBException {

		int planDepartureTime = 0;
		List<ActionWrapper> actions = new ArrayList<>();

		//TODO wierd
		ActionWrapper startNode;
		double startNodeServiceStart = vertexServiceStart.get(new VertexCarKey(darpInstance.getStartNode(), vehicle)).get(GRB.DoubleAttr.X);
		double endNodeServiceStart = vertexServiceStart.get(new VertexCarKey(darpInstance.getEndNode(), vehicle)).get(GRB.DoubleAttr.X);

		if (startNodeServiceStart <= endNodeServiceStart){
			startNode = new ActionWrapper(darpInstance.getStartNode());
		}
		else{
			startNode = new ActionWrapper(darpInstance.getEndNode());
		}

		ActionWrapper newAction = getNextAction(startNode.getAction(), vehicle);

		while (newAction != null){
			int serviceStartTime = (int) Math.round(vertexServiceStart.get(new VertexCarKey(newAction.getAction(), vehicle)).get(GRB.DoubleAttr.X));

			if (actions.size() > 0){
				//set departure time to previous action
				ActionWrapper previousAction = actions.get(actions.size()-1);
				newAction.setArrivalTime(countArrivalTime(newAction.getAction(), previousAction));
			}
			else{
				planDepartureTime = (int) Math.round(vertexServiceStart.get(new VertexCarKey(startNode.getAction(), vehicle)).get(GRB.DoubleAttr.X));
				startNode.setDepartureTime(planDepartureTime);
				newAction.setArrivalTime(countArrivalTime(newAction.getAction(), startNode));
			}


			newAction.setDepartureTime(serviceStartTime + newAction.getAction().getServiceTime());
			actions.add(newAction);
			newAction = getNextAction(newAction.getAction(), vehicle);
		}

		if (actions.size() > 0){
			actions.remove(actions.size()-1);
		}

		int planArrivalTime = (int) Math.round(vertexServiceStart.get(new VertexCarKey(darpInstance.getEndNode(), vehicle)).get(GRB.DoubleAttr.X));

		//TODO count cost
		int cost = countCost(actions);

		Plan plan = new Plan(cost, vehicle, planDepartureTime, planArrivalTime, actions);

		return plan;
	}

	private ActionWrapper getNextAction(Action start, Vehicle vehicle) throws GRBException {
		for (Action action : darpInstance.getVAction()) {
			if (vars.containsKey(new VarKey(start, action, vehicle))){
				GRBVar var = vars.get(new VarKey(start, action, vehicle));
				if (Math.round(var.get(GRB.DoubleAttr.X)) == 1){
					if (action != start){
						return new ActionWrapper(action);
					}
				}
			}
		}
		return null;
	}


	private int countArrivalTime(Action newAction, ActionWrapper previousAction){
		int arrivalTime = previousAction.getDepartureTime() + darpInstance.getTravelTimeProvider().getTravelTime(previousAction.getAction().getNode(), newAction.getNode());
		return arrivalTime;
	}

	public int countCost(List<ActionWrapper> actions){
		int cost = 0;
		for (int i = 0; i < actions.size() - 1; i++) {
			cost += darpInstance.getTravelTimeProvider().getTravelTime(actions.get(i).getAction().getNode(), actions.get(i+1).getAction().getNode());
		}
		if (actions.size() > 0){
			cost += darpInstance.getTravelTimeProvider().getTravelTime(darpInstance.getStartNode().getNode(), actions.get(0).getAction().getNode());
			cost += darpInstance.getTravelTimeProvider().getTravelTime(darpInstance.getEndNode().getNode(), actions.get(actions.size()-1).getAction().getNode());
		}

		return cost;
	}

}
